<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $depan = $request['fname'];
        $belakang = $request['lname'];

        return view('welcome', compact('depan','belakang'));
    }
}
