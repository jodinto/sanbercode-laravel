@extends('layout.master')
@section('title')
Buat Account Baru
@endsection
@section('content')
    <form action="/welcome" method="post">
      @csrf
      <h3>Sign Up Form</h3>
      <label for="first_name">First name:</label><br />
      <input required type="text" id="first_name" name="fname" /><br />
      <br />
      <label for="last_name">Last name:</label><br />
      <input required type="text" id="last_name" name="lname"/><br />
      <br />
      <label for="gender">Gender:</label><br />
      <input required type="radio" id="male" name="gender" /><label for="male">Male</label><br />
      <input required type="radio" id="female" name="gender" /><label for="female">Female</label><br />
      <input required type="radio" id="other" name="gender" /><label for="other">Other</label><br />
      <br />
      <label for="nationality">Nationality:</label>
      <select id="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
      </select>
      <br /><br />
      <label for="language">Language Spoken:</label>
      <br />
      <input type="checkbox" id="bahasa_indonesia" /><label for="bahasa_indonesia">Bahasa Indonesia</label><br />
      <input type="checkbox" id="english" /><label for="english">English</label><br />
      <input type="checkbox" id="arabic" /><label for="arabic">Arabic</label><br />
      <input type="checkbox" id="japanese" /><label for="japanese">Japanese</label><br />
      <br />
      <label for="bio">Bio:</label><br />
      <textarea id="bio" cols="30" rows="10"></textarea><br />
      <button type="submit">Sign Up</button>
    </form>
@endsection
