<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function(){
    return view ('table');
});

Route::get('/data-table', function(){
    return view ('data-table');
});

// CRUD Cast
// Create
// Route untuk menuju ke form yang digunakan untuk membuat data pemain film baru
Route::get('/cast/create', 'CastController@create');
// menyimpan data baru ke tabel Cast
Route::post('/cast', 'CastController@store');

// Read
// Route untuk menampilkan list data para pemain film
Route::get('/cast', 'CastController@index');
// Route untuk detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

// Update
// Route untuk mengarah ke halaman form yang digunakan untuk mengedit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Untuk menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', 'CastController@update');

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');